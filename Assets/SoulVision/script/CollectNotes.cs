using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectNotes : MonoBehaviour, ICollectable
{
    public ScriptableObjects dati;
    public Notes not;

    private void Start()
    {
        not = FindObjectOfType<Notes>();
    }
    public void Collection()
    {
        not.AddNotes(dati, this.gameObject);
    }
}
