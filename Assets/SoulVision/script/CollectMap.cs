using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectMap : MonoBehaviour, ICollectable
{
    [SerializeField] GameObject pezzoMap;

    public void Collection()
    {
        pezzoMap.SetActive(true);
        Destroy(this.gameObject);
    }
}
