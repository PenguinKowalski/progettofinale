using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Inventory : MonoBehaviour
{
   public  List<ScriptableObjects> listaItems;
   
    public void AddItem(ScriptableObjects newItem)
    {
        if (listaItems == null) listaItems = new List<ScriptableObjects>();

        if (!listaItems.Contains(newItem))
        {
            listaItems.Add(newItem);
        }
    }
    public void RemoveItem(ScriptableObjects removeItem)
    {
        if (listaItems == null) listaItems = new List<ScriptableObjects>();

        if (listaItems.Contains(removeItem))
        {
            listaItems.Remove(removeItem);
        }
    }
    public List<ScriptableObjects> GetItems()
    {
        if (listaItems == null) listaItems = new List<ScriptableObjects>();
        return listaItems;
    }

}
