using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractive
{
    void Interacte(ScriptableObjects item);
}

public class Interactable : MonoBehaviour, IInteractive
{
    [SerializeField] ScriptableObjects oggettoKey;  //oggetto da usare per l'enigma
    [SerializeField] GameObject porta, DestroyTrigger, panelTesti;
    private SwitchPuzzle sw;
    private bool flag = false;


    private void Start()
    {
        sw = FindObjectOfType<SwitchPuzzle>();
    }

    private void Update()
    {
        if (flag)
        {
            foreach (var p in porta.GetComponentsInChildren<MeshRenderer>())
            {
                p.enabled = false;
            }
            foreach (var pcoll in porta.GetComponentsInChildren<Collider>())
            {
                pcoll.enabled = false;
            }
        }
    }

    public void Interacte(ScriptableObjects item)
    {
        if (oggettoKey == item)
        {
            sw.Back();
            Destroy(DestroyTrigger);
            StartCoroutine(Wait());
        }
        panelTesti.SetActive(false);
    }

    public IEnumerator Wait()
    {
        yield return new WaitForSeconds(0.5f);
        flag = true;
    }
}
