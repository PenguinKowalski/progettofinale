using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectRunes : MonoBehaviour, ICollectable
{
    public ScriptableObjects dati;
    public Runes run;
    [SerializeField] Image runa;

    private void Start()
    {
        run = FindObjectOfType<Runes>();
    }
    public void Collection()
    {
        //run.AddRunes(dati, this.gameObject);
        runa.enabled = true;
        runa.GetComponent<UICellaRune>().Setup(dati, run);
        Destroy(this.gameObject);
    }
}