using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class InventoryPuzzleHUD : MonoBehaviour
{
    Inventory inventoryData;
    [SerializeField] public GameObject inventory;

    public Image image;
    public ScriptableObjects itemEquipped;



    public Transform griglia;
    public GameObject prefabCanvas;

    private void Start()
    {
        SetItemhand(null);
        inventoryData = FindObjectOfType<Inventory>();
    }


    private void Update()
    {
        if (itemEquipped != null)
        {
            image.transform.position = Input.mousePosition;
        }
    }
    public void StartSetup()
    {
        inventory.SetActive(true);
        List<ScriptableObjects> oggetti = inventoryData.GetItems();

        for (int i = griglia.childCount - 1; i >= 0; i--)
        {
            Destroy(griglia.GetChild(i).gameObject);
        }

        foreach (ScriptableObjects oggetto in oggetti)
        {
            GameObject cella = GameObject.Instantiate(prefabCanvas, griglia);
            cella.GetComponent<UICellaInventarioPuzzle>().Setup(oggetto, this);
        }
    }

    public void SetItemhand(ScriptableObjects item)
    {
        itemEquipped = item;

        if (item!=null)
        image.sprite = item.Logo;

        if (itemEquipped != null)
            image.gameObject.SetActive(true);
        else
        {
            image.gameObject.SetActive(false);
        }
    }

}
