using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class InventoryHUD : MonoBehaviour
{
    Inventory inventoryData;
    [SerializeField] GameObject soulmode, inventory, outline, panelInv, runes, notes;
    private GameObject modelloInstant, modelRotate;
    private float Rotatespeed = 10f;
    [SerializeField] TextMeshProUGUI descrizione;
    [SerializeField] ZoomInv zoom;
    [SerializeField] Camera main;

    [SerializeField] SoulVison stato;
    private FirstPersonController fps;
    public Transform griglia, grigliaPuzzle;
    public GameObject prefabCanvas, prefabSpritePuzzle;
    bool flag = false, isRotating = false;
    int soul;


    private void Start()
    {
        fps = FindObjectOfType<FirstPersonController>();
        inventoryData = FindObjectOfType<Inventory>();
    }

    private void StartSetup()
    {
        List<ScriptableObjects> oggetti = inventoryData.GetItems();

        for (int i= griglia.childCount-1; i>=0; i--)
        {
            Destroy(griglia.GetChild(i).gameObject);
        }

        foreach(ScriptableObjects oggetto in oggetti)
        {
            GameObject cella = GameObject.Instantiate(prefabCanvas, griglia);
            cella.GetComponent<UICellaInvetario>().Setup(oggetto, this);
        }
    }
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I) && !flag)
        {
            inventory.SetActive(true);
            fps.hud.SetActive(false);
            StartSetup();
            main.enabled = false;
            if (fps.lockCursor)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            if (stato.statoAttuale == State.soulVision)
            {
                soulmode.SetActive(false);
                soul = 1;
            }
            flag = true;
        }
        else if (Input.GetKeyDown(KeyCode.I) && flag)
        {
            inventory.SetActive(false);
            fps.hud.SetActive(true);
            if (fps.lockCursor)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (soul == 1)
            {
                soulmode.SetActive(true);
                soul = 0;
            }
            main.enabled = true;
            flag = false;
        }

        if (Input.GetMouseButton(0) && isRotating)
        {
            float xRot = Input.GetAxis("Mouse X") * Rotatespeed;
            float yRot = Input.GetAxis("Mouse Y") * Rotatespeed;
            modelRotate.transform.Rotate(Vector3.down, xRot);
            modelRotate.transform.Rotate(Vector3.right, yRot);
        }
    }

    public void AddObject(ScriptableObjects dati, GameObject ob)
    {
        GameObject cella = GameObject.Instantiate(prefabCanvas, griglia);
        cella.GetComponent<UICellaInvetario>().Setup(dati, this);
        Destroy(ob);
    }

   

    public void Click(ScriptableObjects so)
    {
        GameObject clone = Instantiate(so.Modello, new Vector3(1532f, 789f, -92.12431f), Quaternion.Euler(new Vector3(0, 0, 0)));
        clone.transform.SetParent(outline.transform);
        if (modelloInstant != null)
        {
            Destroy(modelloInstant);
        }
        modelloInstant = clone;
        clone.transform.localScale = new Vector3(350f, 350f, 250f);
        descrizione.text = so.Descrizione;
    }

    public void ZoomModel()
    {
        GameObject cloneZoom = Instantiate(modelloInstant, new Vector3(969f, 552f, -446f), Quaternion.Euler(0, 0, 0));
        cloneZoom.transform.localScale = new Vector3(400f, 400f, 400f);
        modelRotate = cloneZoom;
        panelInv.SetActive(false);
        isRotating = true;
    }

    public void Back()
    {
        panelInv.SetActive(true);
        zoom.panelZoom.SetActive(false);
        Destroy(modelRotate);
        isRotating = false;
    }

    public void NotesPanel()
    {
        notes.SetActive(true);
        panelInv.SetActive(false);
    }

    public void RunesPanel()
    {
        runes.SetActive(true);
        panelInv.SetActive(false);
    }

}
