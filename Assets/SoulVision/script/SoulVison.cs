using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum State { normal, soulVision}
public class SoulVison : MonoBehaviour
{
    public State statoAttuale;
    [SerializeField] Color soulvisionColor;
    [SerializeField] GameObject canvas, soulvisionObj;
    public bool flag = false;

    private void Start()
    {
        foreach (var Soul in soulvisionObj.GetComponentsInChildren<MeshRenderer>())
        {
            Soul.enabled = false;
        }
        foreach (var soulColl in soulvisionObj.GetComponentsInChildren<Collider>())
        {
            soulColl.enabled = false;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !flag)
        {
            StatoSVision();
        }
        else if (Input.GetKeyDown(KeyCode.Space) && flag)
        {
            StatoNorm();
        }
    }

    public void StatoNorm()
    {
        statoAttuale = State.normal;
        canvas.SetActive(false);
        foreach (var Soul in soulvisionObj.GetComponentsInChildren<MeshRenderer>())
        {
            Soul.enabled = false;
        }
        foreach (var soulColl in soulvisionObj.GetComponentsInChildren<Collider>())
        {
            soulColl.enabled = false;
        }
        flag = false;
    }

   public void StatoSVision()
    {
        statoAttuale = State.soulVision;
        canvas.SetActive(true);
        foreach (var Soul in soulvisionObj.GetComponentsInChildren<MeshRenderer>())
        {
            Soul.enabled = true;
        }
        foreach (var soulColl in soulvisionObj.GetComponentsInChildren<Collider>())
        {
            soulColl.enabled = true;
        }
        flag = true;
    }
}
