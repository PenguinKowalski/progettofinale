using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Notes : MonoBehaviour
{
    public GameObject prefabCellaNote, panelinv, notesPanel, runePanel;
    public Transform griglia;
    [SerializeField] TextMeshProUGUI descrizione;

    public void AddNotes(ScriptableObjects dati, GameObject ob)
    {
        GameObject cella = GameObject.Instantiate(prefabCellaNote, griglia);
        cella.GetComponent<UICellaNotes>().Setup(dati, this);
        Destroy(ob);
    }

    public void Click(ScriptableObjects so)
    {
        descrizione.text = so.Descrizione;
    }

    public void InvPanel()
    {
        panelinv.SetActive(true);
        notesPanel.SetActive(false);
    }

    public void RunPanel()
    {
        runePanel.SetActive(true);
        notesPanel.SetActive(false);
    }
}
