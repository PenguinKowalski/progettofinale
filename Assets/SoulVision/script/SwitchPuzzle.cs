using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SwitchPuzzle : MonoBehaviour
{
    [SerializeField] GameObject cameraPuzzle;
    [SerializeField] public GameObject panelTesti;
    [SerializeField] CanvasGroup fade;
    [SerializeField] TextMeshProUGUI testo;
    private bool flag = false;
    private FirstPersonController fps;
    private InventoryPuzzleHUD hud;

    private void Start()
    {
        fps = FindObjectOfType<FirstPersonController>();
        hud = FindObjectOfType<InventoryPuzzleHUD>();
    }

    private void Update()
    {
        if(Input.GetKeyDown(KeyCode.E) && flag)
        {
            Fade();
        }
    }
    public void OnTriggerEnter(Collider other)
    {
        if(other.gameObject.tag == "Player")
        {
            panelTesti.SetActive(true);
            testo.text = "Press 'E' to interact";
            flag = true;
        }
    }

    public void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            panelTesti.SetActive(false);
            flag = false;
        }
    }

    public void Fade()
    {
        LeanTween.alphaCanvas(fade, 1f, 0.5f).setOnComplete(() => { 
        cameraPuzzle.SetActive(true);
        panelTesti.SetActive(false);
        fps.hud.SetActive(false);
        hud.StartSetup();
        LeanTween.alphaCanvas(fade, 0f, 0.5f); }
        );
        if (fps.lockCursor)
        {
            Cursor.lockState = CursorLockMode.None;
        }
        fps.rb.constraints = RigidbodyConstraints.FreezeAll;
        flag = false;
    }

    public void Back()
    {
        LeanTween.alphaCanvas(fade, 1f, 0.5f).setOnComplete(() => {
            cameraPuzzle.SetActive(false);
            panelTesti.SetActive(true);
            fps.hud.SetActive(true);
            LeanTween.alphaCanvas(fade, 0f, 0.5f);
        }
       );
        if (fps.lockCursor)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        fps.rb.constraints = RigidbodyConstraints.None;
        fps.rb.constraints = RigidbodyConstraints.FreezeRotation;
        flag = true;
        hud.image.gameObject.SetActive(false);
        hud.inventory.SetActive(false);
    }
}
