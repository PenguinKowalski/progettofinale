using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorAnimScript : MonoBehaviour
{
    public GameObject Door;

    private void OnTriggerEnter(Collider other)
    {
        LeanTween.moveLocalZ(Door, 1.3f, 0.5f);
    }

    private void OnTriggerExit(Collider other)
    {
        LeanTween.moveLocalZ(Door, 0, 0.5f);
    }
}
