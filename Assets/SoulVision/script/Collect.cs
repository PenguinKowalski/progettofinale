using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public interface ICollectable
{
    void Collection();
}
public class Collect : MonoBehaviour, ICollectable
{
    public ScriptableObjects dati;
    public Inventory inv;

    private void Start()
    {
        inv = FindObjectOfType<Inventory>();
    }
    public void Collection()
    {
        inv.AddItem(dati);
        Destroy(this.gameObject);
    }
}
