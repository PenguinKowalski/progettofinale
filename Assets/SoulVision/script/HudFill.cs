using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HudFill : MonoBehaviour
{
    public Image coolDown;
    private float timeCool = 10.0f;
    private float timeFill = 20.0f;
    private FirstPersonController fps;
    private SoulVison sv;

    private void Start()
    {
        fps = FindObjectOfType<FirstPersonController>();
        sv = FindObjectOfType<SoulVison>();
    }

    // Update is called once per frame
    void Update()
    {
        if (fps.isSprinting || sv.statoAttuale == State.soulVision)
        {
            coolDown.fillAmount -= 1.0f / timeCool * Time.deltaTime;
            if(coolDown.fillAmount == 0)
            {
                fps.isSprinting = false;
                sv.StatoNorm();
            }
        }
        if (!fps.isSprinting && sv.statoAttuale != State.soulVision)
        {
            coolDown.fillAmount += 1.0f / timeFill * Time.deltaTime;
        }
    }
}
