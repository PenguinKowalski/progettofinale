using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Raycaster : MonoBehaviour
{
    bool flag = false;
    private FirstPersonController raycast;

    private void Start()
    {
        raycast = FindObjectOfType<FirstPersonController>();
    }
    void Update()
    {
        if (Physics.Raycast(raycast.playerCamera.transform.position, raycast.playerCamera.transform.forward, out raycast.ray, raycast.raylength))
        {
            flag = true;
            if (Input.GetKeyDown(KeyCode.E) && flag)
            {
                ICollectable coll = raycast.ray.collider.GetComponent<ICollectable>();
                if (coll != null)
                {
                    coll.Collection();
                }
            }
        }
    }
}
