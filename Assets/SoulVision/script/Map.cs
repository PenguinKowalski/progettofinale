using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Map : MonoBehaviour
{
    [SerializeField] GameObject mapCanvas, soulmode;
    bool flag = false;
    [SerializeField] Camera main;
    [SerializeField] SoulVison stato;
    private FirstPersonController fps;
    int soul;

    private void Start()
    {
        fps = FindObjectOfType<FirstPersonController>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Tab) && !flag)
        {
            mapCanvas.SetActive(true);
            main.enabled = false;
            if (fps.lockCursor)
            {
                Cursor.lockState = CursorLockMode.None;
            }
            if (stato.statoAttuale == State.soulVision)
            {
                soulmode.SetActive(false);
                soul = 1;
            }
            flag = true;
        }
        else if (Input.GetKeyDown(KeyCode.Tab) && flag)
        {
            mapCanvas.SetActive(false);
            if (fps.lockCursor)
            {
                Cursor.lockState = CursorLockMode.Locked;
            }
            if (soul == 1)
            {
                soulmode.SetActive(true);
                soul = 0;
            }
            main.enabled = true;
            flag = false;
        }
    }
}
