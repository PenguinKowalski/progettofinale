using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrossingWalls : MonoBehaviour
{
    private SoulVison stato;
    [SerializeField] Collider colliderWall;

    private void Start()
    {
        stato = FindObjectOfType<SoulVison>();
    }
    private void Update()
    {
        switch (stato.statoAttuale)
        {
            case (State.normal):
                colliderWall.isTrigger = false;
                break;
            case (State.soulVision):
                colliderWall.isTrigger = true;
                break;
        }
    }
}
