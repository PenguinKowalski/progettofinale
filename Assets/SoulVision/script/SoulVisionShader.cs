using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoulVisionShader : MonoBehaviour
{
    [SerializeField] Material SVcollectable, OriginalMat; 
    private SoulVison SV;
    void Start()
    {
        SV = FindObjectOfType<SoulVison>();
        OriginalMat = this.gameObject.GetComponent<MeshRenderer>().material;
    }

    // Update is called once per frame
    void Update()
    {
        if (SV.statoAttuale == State.soulVision)
        {
            this.gameObject.GetComponent<MeshRenderer>().material = SVcollectable; 
        } 
        else if (SV.statoAttuale == State.normal)
        {
            this.gameObject.GetComponent<MeshRenderer>().material = OriginalMat;
        }
    }
}
