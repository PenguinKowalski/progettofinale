using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellaInvetario : MonoBehaviour
{
    private InventoryHUD i;
    [SerializeField]
    private Image sprite;
    private ScriptableObjects dato;
    
    public void Setup(ScriptableObjects dati, InventoryHUD inv)
    {
        dato = dati;
        sprite.sprite = dati.Logo;
        i = inv;
    }
    public void Click()
    {
        i.Click(dato); 
    }
}
