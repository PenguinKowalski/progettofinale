using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class Runes : MonoBehaviour
{
    public GameObject outline, prefabSprite, panelinv, runePanel, notesPanel;
    public Transform griglia;
    [SerializeField] TextMeshProUGUI descrizione;
    [SerializeField] Image prefabZoomRune;

    /*public void AddRunes(ScriptableObjects dati, GameObject ob)
    {
        GameObject cella = GameObject.Instantiate(prefabSprite, griglia);
        cella.GetComponent<UICellaRune>().Setup(dati, this);
        Destroy(ob);
    }*/

    public void Click(ScriptableObjects so)
    {
        prefabZoomRune.sprite = so.Logo;
        Instantiate(prefabZoomRune, outline.transform);
        descrizione.text = so.Descrizione;
    }

    public void InvPanel()
    {
        panelinv.SetActive(true);
        runePanel.SetActive(false);
    }

    public void NotPanel()
    {
        notesPanel.SetActive(true);
        runePanel.SetActive(false);
    }
}
