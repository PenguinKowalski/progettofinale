using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellaRune : MonoBehaviour
{
    private Runes r;
    /*[SerializeField]
    private Image sprite;*/
    private ScriptableObjects dato;

    public void Setup(ScriptableObjects dati, Runes run)
    {
        dato = dati;
        //sprite.sprite = dati.Logo;
        r = run;
    }

    public void Click()
    {
        r.Click(dato);
    }
}
