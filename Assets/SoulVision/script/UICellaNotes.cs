using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UICellaNotes : MonoBehaviour
{
    private Notes n;
    [SerializeField] TextMeshProUGUI nome;
    private ScriptableObjects dato;

    public void Setup(ScriptableObjects dati, Notes not)
    {
        dato = dati;
        nome.text = dati.Nome;
        n = not;
    }

    public void Click()
    {
        n.Click(dato);
    }
}
