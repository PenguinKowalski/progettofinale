using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraRaycastPuzzle : MonoBehaviour
{

    [SerializeField] Camera cameraPuzzle;
    private ScriptableObjects itemToUse;
    Vector3 worldposition;
    private InventoryPuzzleHUD hud;

    private void Start()
    {
        hud = FindObjectOfType<InventoryPuzzleHUD>();
    }

    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            RaycastHit hit;
            Vector3 camPosition = cameraPuzzle.transform.localPosition;
            Vector3 mousez = cameraPuzzle.WorldToScreenPoint(new Vector3(0, 0, Input.mousePosition.z));
            Vector3 origin = cameraPuzzle.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, Input.mousePosition.z));
           
            var hit2D = Physics.Raycast(new Vector3(origin.x, origin.y, origin.z), Vector3.left, out hit, 100);
            if (hit2D)
            {
                itemToUse = hud.itemEquipped;
                IInteractive inte = hit.collider.GetComponent<IInteractive>();
                if (inte != null)
                {
                    inte.Interacte(itemToUse);
                }
            }
        }
    }
}
