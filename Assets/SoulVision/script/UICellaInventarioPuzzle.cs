using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICellaInventarioPuzzle : MonoBehaviour
{
    private InventoryPuzzleHUD i;
    [SerializeField]
    private Image sprite;
    private ScriptableObjects dato;

    public void Setup(ScriptableObjects dati, InventoryPuzzleHUD inv)
    {
        dato = dati;
        sprite.sprite = dati.Logo;
        i = inv;
    }

    public void Click()
    {
        i.SetItemhand(dato);
    }
}
