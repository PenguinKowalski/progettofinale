using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoomInv : MonoBehaviour
{
    [SerializeField] public GameObject panelZoom;
    private InventoryHUD inv;

    private void Start()
    {
        inv = FindObjectOfType<InventoryHUD>();
    }

    public void Zoom()
    {
        panelZoom.SetActive(true);
        inv.ZoomModel();
    }

}
