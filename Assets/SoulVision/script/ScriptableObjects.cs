using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

[CreateAssetMenu(fileName = "Collectable", menuName = "Collect/CreateObject", order = 1)]
public class ScriptableObjects : ScriptableObject
{
    [SerializeField] string nome;
    [SerializeField] string description;
    [SerializeField] Sprite logo;
    [SerializeField] GameObject modello3D;


    public string Nome
    {
        get { return nome; }
    }

    public string Descrizione
    {
        get { return description; }
    }

    public Sprite Logo
    {
        get { return logo; }
    }

    public GameObject Modello
    {
        get { return modello3D; }
    }
}
