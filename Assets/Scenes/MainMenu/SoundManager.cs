using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class SoundManager : MonoBehaviour
{
    public static SoundManager Instance;
    public float musicVolume, soundEffectsVolume;

    [SerializeField] private AudioSource _musicSource, _effectsSource;
    [SerializeField] private AudioMixerGroup musicMixerGroup;
    [SerializeField] private AudioMixerGroup soundEffectMixerGroup;

    [SerializeField] Slider sliderSfx, sliderMusic;
    void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    private void Start()
    {
        musicMixerGroup.audioMixer.GetFloat("MUSIC", out musicVolume);
        soundEffectMixerGroup.audioMixer.GetFloat("SFX", out soundEffectsVolume);

        sliderSfx.value = soundEffectsVolume;
        sliderMusic.value = musicVolume;



    }

    public void PlaySound(AudioClip clip)
    {
        _effectsSource.PlayOneShot(clip);
    }

    public void ChangeMasterVolume(float value)
    {
        AudioListener.volume = value;
    }

    public void OnMusicSliderValueChange(float value)
    {
        musicVolume = value;     
        musicMixerGroup.audioMixer.SetFloat("MUSIC", musicVolume);
    }
    public void OnSoundEffectsSliderValueChange(float value)
    {
        soundEffectsVolume = value;
        soundEffectMixerGroup.audioMixer.SetFloat("SFX", soundEffectsVolume);
    }
}

